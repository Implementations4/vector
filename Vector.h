

template<typename T>//используем шаблоны
class Vector {
    private:
    T* _arr;//указатель на массив 
    size_t _size{};
    size_t _capacity{};

    public:
    Vector():_arr(new T(1)),_capacity(1){}
    ~Vector() {delete[] _arr;}

    Vector(Vector& other){
        if (this != &other) {
            delete[] _arr;
     		_arr = new T[other._capacity];
            for (size_t i = 0; i < other._size; ++i) 
                _arr[i] = other._arr[i];
            _size = other._size;
            _capacity = other._capacity;
        }
    }
    
    Vector(Vector && other){
         if (this != &other){
            delete[] _arr;
            _arr=other._arr;
            _size=other._size;
            _capacity=other._capacity;
            other._arr=nullptr;
            other._size=other._capacity=0;
         }
    }
	

/*interface*/
   bool empty(){
    if(_size == 0) 
        return true;
    else
        return false;
   }

  size_t size(){return _size;}
  size_t capasity(){return _capacity;}//Метод получения максимального размера вектора

  void addMemory(){
    _capacity *= 2;
    T* tmp = _arr;
    _arr=new T[_capacity];
    for(size_t i=0; i<_size;i++)
        _arr[i]=tmp[i];

    delete[] tmp;
  }
    
   void push_back(const T& value){

    if(_size>=_capacity)
        addMemory();
        _arr[_size++] = value;

  }

 void remove(size_t index){
    for(size_t i=index+1; i<_size;i++){
        _arr[i-1]=_arr[i];
    }      
    --_size;

 }

 /*operators*/

//обычную ссылку
 T& operator[](size_t index){
    return _arr[index];
 }
 //возвращает конст ссылку 
 const T& operator[](size_t index) const{
    return _arr[index];
 }

 Vector& operator=(Vector& other) {
        if (this != &other) {
            delete[] _arr;
     		_arr = new T[other._capacity];
            for (size_t i = 0; i < other._size; ++i) 
                _arr[i] = other._arr[i];
            _size = other._size;
            _capacity = other._capacity;
        }
    return *this;
}

Vector& operator=(Vector&& other){
          if (this != &other){
            delete[] _arr;
            _arr=other._arr;
            _size=other._size;
            _capacity=other._capacity;
            other._arr=nullptr;
            other._size=other._capacity=0;
         }
         return *this;

}

Vector& operator() (const T& value) { 
    push_back(value);
   return *this;
   // return (m_counter += i); 

    }

Vector& operator+(Vector&& other){
    if(this->_size==other._size){
         for (size_t i = 0; i < other._size; ++i) 
           _arr[i] += other._arr[i]; 
    }
}

const Vector& operator+(const Vector&& other){
      if(this->_size==other._size){
         for (size_t i = 0; i < other._size; ++i) 
           _arr[i] += other._arr[i]; 
    }

}






/*iterator*/
//возврат указателя так как итератор работает с указ.
T* begin(){
    return &_arr[0];
}
const T* begin()const {
    return &_arr[0];
}

T* end(){
    return &_arr[_size];
}

const T* end() const{
    return &_arr[_size];
}

        
    

};


template<typename T>
inline std::ostream& operator<<(std::ostream& os,  Vector<T>& vec) {
    for (size_t i = 0; i < vec.size(); ++i) os << vec[i] << " ";
    return os;
}

template<typename T>
Vector<T>& operator+(Vector<T>& left, Vector<T>& right) {
     if(left.size()==right.size()){
         for (size_t i = 0; i < left.size(); ++i) 
            left[i] += right[i];
    }
    return left;

}

template<typename T>
Vector<T>& operator+=(Vector<T>& left,  Vector<T>& right) {
     if(left.size()==right.size()){
         for (size_t i = 0; i < left.size(); ++i) 
            left[i] += right[i];
    }
    return left;
}

